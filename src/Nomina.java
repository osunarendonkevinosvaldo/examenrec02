/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hp
 */
public class Nomina {
    private int numRecibo;
    private String nombre;
    private int puesto;
    private int nivel;
    private int diasTrabajados;
    
    
    public Nomina(){
    this.numRecibo = 0;
    this.nombre = "";
    this.puesto = 0;
    this.nivel = 0;
    this.diasTrabajados = 0;
    }
    
    public Nomina(int numRecibo, String nombre, int puesto, int nivel, int diasTrabajados) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.puesto = puesto;
        this.nivel = nivel;
        this.diasTrabajados = diasTrabajados;
    }
     public Nomina(Nomina x) {
        this.numRecibo = x.numRecibo;
        this.nombre = x.nombre;
        this.puesto = x.puesto;
        this.nivel = x.nivel;
        this.diasTrabajados = x.diasTrabajados;
     }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(int diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }
     
    public float calcularPago(){
        float pago = 0.0f;
        if (this.puesto == 1 ){
            pago = this.diasTrabajados * 100;
        }
        if (this.puesto == 2 ){
            pago = this.diasTrabajados * 200;
        }
        if (this.puesto == 3 ){
            pago = this.diasTrabajados * 300;
        }
        return pago;
    }
    public float calcularImpuesto(){
        float impuesto = 0.0f;
        if (this.nivel == 1){
            impuesto = (this.calcularPago()*0.05f);
        }
        if (this.nivel == 2){
            impuesto = (this.calcularPago()*0.03f);
        }
        return impuesto;
    }
    public float calcularTotalPagar(){
        float totalPagar = 0.0f;
        totalPagar = (this.calcularPago()- this.calcularImpuesto());
        return totalPagar;
    }
    
}

